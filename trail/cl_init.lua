include('shared.lua')


function ENT:getTrailNode(n)
  if self.nodes[n] == nil then return nil end
  return self.nodes[n]
end

-- Scale renderbound with trail.
function ENT:updateBoundingBox()
  local renderOrigin = self:GetPos()
	local renderMins = renderOrigin
	local renderMaxs = renderOrigin
	local maxs, mins = Vector()

  for i=1, #self.nodes do
    local tNode = self:getTrailNode(i)
    if tNode == nil then return end

    -- Node width
    local nWidth = Vector(tNode.width,tNode.width,tNode.width)
    mins = tNode.pos - nWidth
    maxs = tNode.pos + nWidth

    -- Gets the vector that is the furthest out.
    renderMins = Vector(math.min(renderMins.x, mins.x),math.min(renderMins.y, mins.y),math.min(renderMins.z, mins.z))
    renderMaxs = Vector(math.max(renderMaxs.x, maxs.x),math.max(renderMaxs.y, maxs.y),math.max(renderMaxs.z, maxs.z))
  end

  -- Get localvector
	renderMins = renderMins - renderOrigin
  renderMaxs = renderMaxs - renderOrigin

  self:SetRenderBounds(renderMins, renderMaxs)
end


function ENT:updateTrails()
  local lifetime = self:GetlifeTime()
  local endWidth = self:GetendWidth()
  local startWidth = self:GetstartWidth()
  local startFade = self:GetstartFade()

  for i=1, #self.nodes do
    local tNode = self:getTrailNode(i)
    if tNode == nil then return end

    local dieTime = ( CurTime( ) - tNode.time ) / lifetime
    dieTime = math.Clamp( dieTime , 0, 1 )

    local liveTime = ( CurTime( ) - tNode.time ) / startFade
    liveTime = math.Clamp( liveTime , 0, 1 )

    -- Check if the node can fade out
    if tNode.fadeout == false then
      tNode.alpha = Lerp(liveTime, tNode.alpha, 255)
    else
      if lifetime != 0 then
        tNode.alpha = Lerp(dieTime*startFade, tNode.alpha, 0)
      end
    end

    if tNode.alpha >= 254 then
      tNode.fadeout = true
    end

    -- Doesn't fade out trail if lifetime is 0
    if lifetime != 0 then
      tNode.width = Lerp(dieTime, tNode.width, endWidth)
    end

    -- Node width can't be greater than starwidth
    if tNode.width > startWidth then
      tNode.width = startWidth
    end

    -- Remove unnecessary nodes
    if math.Round(tNode.alpha) == 0 then
      table.remove(self.nodes,i)
    end

    if endWidth < startWidth then
      if math.Round(tNode.width) <= endWidth+0.1 then
        table.remove(self.nodes,i)
      end
    else
      if math.Round(tNode.width) >= endWidth-0.1 then
        table.remove(self.nodes,i)
      end
    end
  end

  if self:GetReset() then
    self:SetReset(false)
    self.nodes = {}
    self.pLast = 0
    self.updatetime = 0
  end
end

function ENT:Initialize()
  self:SetRenderAngles( Angle(0,0,0) )
  self.updatetime = 0
  self.pLast = 0 -- Last node
  self.nodes = {}
end

function ENT:Think()

  if self.updatetime > CurTime() then return end -- Update time!
  self.pLast = #self.nodes
  self:updateBoundingBox()

  -- Check to see if this node has a parent
  if IsValid(self:GetParent()) then
    if self:GetParent():GetVelocity():Length() == 0 then -- Avoid unnecessary nodes
      self.updatetime = CurTime() +0.025
      self:updateTrails()
      return
    end
  else -- Do not need to update anything if it's not parented.

    return
  end
  self:updateTrails()

  local newPoint = {}
  newPoint.width = self:GetstartWidth()
  newPoint.time = CurTime()
  if self:GetstartFade() == 0 then
    newPoint.alpha = 255
    newPoint.fadeout = true
  else
    newPoint.alpha = 0
    newPoint.fadeout = false
  end
  newPoint.pos = self:GetPos()+Vector(0,0,5)

  --Make sure the last node still exists
  if self.nodes[self.pLast] == nil then
    table.insert(self.nodes, newPoint)
    self.updatetime = CurTime() +0.025
  else
    -- Avoid making nodes shorter than nodelength
    if self.nodes[self.pLast]["pos"]:Distance(newPoint.pos) > self:GetnodeLength() then
      table.insert(self.nodes, newPoint)
      self.updatetime = CurTime() +0.025
    end
  end
end

function DrawTrail(ent)
  -- Trail Color
  local tc = Color(ent:GettrailColor().x,ent:GettrailColor().y,ent:GettrailColor().z,ent:GettrailAlpha())
  local width = ent:GetstartWidth()
  
  if ent:getTrailNode(#ent.nodes) != nil then
    width = ent:getTrailNode(#ent.nodes)["width"]
  end
  
  render.SetMaterial( Material( ent:GetMaterial() ) )

  -- Creates segmented beam
  render.StartBeam( #ent.nodes +1)
  for k, v in pairs(ent.nodes) do

    render.AddBeam( v.pos , v.width, 1.0, Color(tc.r, tc.g, tc.b, v.alpha ) )
  end
  -- Show the trail beneath the player if it doesn't have startfade
  if ent:GetstartFade() == 0 then
    render.AddBeam( ent:GetPos()+Vector(0,0,5), width, 1.0, tc )
  else
    tc = Color(ent:GettrailColor().x,ent:GettrailColor().y,ent:GettrailColor().z,0)
    render.AddBeam( ent:GetPos()+Vector(0,0,5), width, 1.0, tc )
  end

  render.EndBeam()
end

function ENT:Draw()
  if #self.nodes > 0 then
    DrawTrail(self)
  end
  --[[ Render bound test
  local Min, Max = self:GetRenderBounds()
  Min = Min + self:GetPos()
  Max = Max + self:GetPos()
  local Col, Width = Color( 255, 0, 0, 255 ), 3
  local B1, B2, B3, B4 = Vector(Min.x, Min.y, Min.z), Vector(Min.x, Max.y, Min.z), Vector(Max.x, Max.y, Min.z), Vector(Max.x, Min.y, Min.z)
  local T1, T2, T3, T4 = Vector(Min.x, Min.y, Max.z), Vector(Min.x, Max.y, Max.z), Vector(Max.x, Max.y, Max.z), Vector(Max.x, Min.y, Max.z)

  render.SetMaterial( Material( self:GetMaterial() ) )
  render.DrawBeam( B1, B2, Width, 0, 1, Col )
  render.DrawBeam( B2, B3, Width, 0, 1, Col )
  render.DrawBeam( B3, B4, Width, 0, 1, Col )
  render.DrawBeam( B4, B1, Width, 0, 1, Col )

  render.DrawBeam( T1, T2, Width, 0, 1, Col )
  render.DrawBeam( T2, T3, Width, 0, 1, Col )
  render.DrawBeam( T3, T4, Width, 0, 1, Col )
  render.DrawBeam( T4, T1, Width, 0, 1, Col )

  render.DrawBeam( B1, T1, Width, 0, 1, Col )
  render.DrawBeam( B2, T2, Width, 0, 1, Col )
  render.DrawBeam( B3, T3, Width, 0, 1, Col )
  render.DrawBeam( B4, T4, Width, 0, 1, Col )]]
end
