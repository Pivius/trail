AddCSLuaFile('cl_init.lua')
AddCSLuaFile('shared.lua')
include('shared.lua')
function ENT:Initialize()
  self:SetTrigger( true )
	self:DrawShadow( false )
	self:SetNotSolid( true )
	self:SetNoDraw( false )

  self.Phys = self:GetPhysicsObject()
	if self.Phys and self.Phys:IsValid() then
		self.Phys:Sleep()
		self.Phys:EnableCollisions( false )
	end
  self:SetstartWidth( 5 )
	self:SetendWidth(0 )
  self:SetlifeTime( 5 )
  self:SetstartFade(0 )
  self:SetnodeLength( 10 )
  self:SettrailColor(Vector(255,255,255))
  self:SetMaterial("trails/laser.vmt")
  self:SettrailAlpha(255)
  self:SetReset(false)
end

function ENT:startWidth(w)
  self:SetstartWidth( w )
end

function ENT:endWidth(w)
  self:SetendWidth( w )
end

function ENT:lifeTime(lt)
  self:SetlifeTime( lt )
end

function ENT:startFade(sf)
  self:SetstartFade( sf )
end

function ENT:nodeLength(sl)
  self:SetnodeLength( sl )
end

function ENT:Color(col)
  self:SettrailColor( Vector(col.r,col.g,col.b) )
  self:SettrailAlpha(col.a)
end

function ENT:Reset()
  self:SetReset(true)
end
