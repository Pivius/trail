ENT.Type = "anim"
ENT.Base = "base_anim"
ENT.RenderGroup		= "RENDERGROUP_TRANSLUCENT"
function ENT:SetupDataTables()
	self:NetworkVar( "Float", 0, "startWidth" )
	self:NetworkVar( "Float", 1, "endWidth" )
	self:NetworkVar( "Float", 2, "lifeTime" )
  self:NetworkVar( "Float", 3, "startFade" )
  self:NetworkVar( "Float", 4, "nodeLength" )
  self:NetworkVar( "Vector", 5, "trailColor" )
  self:NetworkVar( "Float", 6, "trailAlpha" )
  self:NetworkVar( "String", 7, "Material" )
  self:NetworkVar( "Bool", 8, "Reset" )
end
